#!/usr/bin/env ruby2.5
# frozen_string_literal: true

require 'eventmachine'
require 'websocket-eventmachine-client'
require 'configatron'
require_relative 'config/config.rb'

# Telling rubocop to up shut.
# rubocop:disable Metrics/BlockLength, Metrics/LineLength, Style/BlockDelimiters
# rubocop:disable Metrics/BlockNesting, Style/IfUnlessModifier

@i = 0


def about

  loop do
    @ws.send "PRIVMSG ##{configatron.channel} :Welcome to Bot Bash, benefiting Toys 4 Tots! Donate at
            http://botbashparty.com"
    @i += 1
    if @i == 1
      break
    end
  end

end

EM.run do

  @ws = WebSocket::EventMachine::Client.connect(host: 'irc-ws.chat.twitch.tv', port: 80, ssl: false)

  @ws.onopen do
    puts 'connected'
    @ws.send 'CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership'
    @ws.send "PASS #{configatron.oauth}"
    @ws.send "NICK #{configatron.nick}"
    @ws.send "JOIN ##{configatron.channel}"
    @ws.send "PRIVMSG ##{configatron.channel} :Hello I'm online."
  end

  @ws.onmessage do |msg|
    if msg.include?('PING') == true
      @ws.send 'PONG :tmi.twitch.tv'
    elsif msg.include?('PRIVMSG')
      msg = msg.downcase
      user_msg_arr = msg.split(' ')
      if user_msg_arr[4..-1].at(0).include?('!tracker')
        @ws.send "PRIVMSG ##{configatron.channel} :Check out these donations! http://tracker.tasbot.net/tracker"
      elsif user_msg_arr[4..-1].at(0).include?('!c')
        @ws.send "PRIVMSG ##{configatron.channel} :Our awesome commentator is dwangoAC when he's not commentating small
                robots, he's having robots play video games."
      elsif user_msg_arr[4..-1].at(0).include?('!about')
        @ws.send "PRIVMSG ##{configatron.channel} :Bot Bash is a charty event benifiting want to donate or learn more
                  about the charity visit http://botbashpary.com."
      elsif user_msg_arr[4..-1].at(0).include('!commands')
        @ws.send "PRIVMSG ##{configatron.channel} :BashBot commands are: !c, !about, !tracker"
      end
    end
  end

  @ws.onclose do |code, reason|
    puts "Disconnected with status code: #{code} #{reason}"
  end
  EM.tick_loop do
    if Time.now.min.zero || Time.now.min == 15 || Time.now.min == 30 || Time.now.min == 45
      about
    end
  end

  @ws.onerror do |error|
    puts "Error: #{error}"
  end

  EventMachine.next_tick do
    puts 'tick'
  end

end

# rubocop:enable Metrics/BlockLength, Metrics/LineLength, Style/BlockDelimiters
# rubocop:enable Metrics/BlockNesting, Style/IfUnlessModifier
